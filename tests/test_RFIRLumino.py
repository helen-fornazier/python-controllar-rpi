from RFIRLumino import RFIRLuminoThmedia
from RFIRLumino import RFIRLuminoBroadlink
from RFIRLumino import RFIRLuminoList
import unittest
from unittest.mock import MagicMock
from unittest.mock import patch
from unittest.mock import mock_open


class TestRFIRLumino(unittest.TestCase):

    # broadlink samsung tv volumeup
    cmd1_broadlink = "26008c009395103a10391039111410151015101411141039113910391114101510151014101510391139101510391015101411141015101510141139101411391039103a10391000060092951139103911391014111410151015101411391039103a10141114101510151014103a103910151039101510151014111410151015103910151039103a1039103a10000d05000000000000000000000000"  # noqa
    pul1_array = [4476, 4537, 487, 1766, 487, 1735, 487, 1735, 517, 609, 487, 639, 487, 639, 487, 609, 517, 609, 487, 1735, 517, 1735, 487, 1735, 517, 609, 487, 639, 487, 639, 487, 609, 487, 639, 487, 1735, 517, 1735, 487, 639, 487, 1735, 487, 639, 487, 609, 517, 609, 487, 639, 487, 639, 487, 609, 517, 1735, 487, 609, 517, 1735, 487, 1735, 487, 1766, 487, 1735, 487, 46776, 4446, 4537, 517, 1735, 487, 1735, 517, 1735, 487, 609, 517, 609, 487, 639, 487, 639, 487, 609, 517, 1735, 487, 1735, 487, 1766, 487, 609, 517, 609, 487, 639, 487, 639, 487, 609, 487, 1766, 487, 1735, 487, 639, 487, 1735, 487, 639, 487, 639, 487, 609, 517, 609, 487, 639, 487, 639, 487, 1735, 487, 639, 487, 1735, 487, 1766, 487, 1735, 487, 1766, 487]  # noqa
    pul1_raw = "4476,4537,487,1766,487,1735,487,1735,517,609,487,639,487,639,487,609,517,609,487,1735,517,1735,487,1735,517,609,487,639,487,639,487,609,487,639,487,1735,517,1735,487,639,487,1735,487,639,487,609,517,609,487,639,487,639,487,609,517,1735,487,609,517,1735,487,1735,487,1766,487,1735,487,46776,4446,4537,517,1735,487,1735,517,1735,487,609,517,609,487,639,487,639,487,609,517,1735,487,1735,487,1766,487,609,517,609,487,639,487,639,487,609,487,1766,487,1735,487,639,487,1735,487,639,487,639,487,609,517,609,487,639,487,639,487,1735,487,639,487,1735,487,1766,487,1735,487,1766,487"  # noqa
    # broadlink samsung tv power
    cmd2_broadlink = "26008c0092961039103a1039101510151014101510151039103a10391015101411141015101510141139101510141114101510151014103a10141139103911391037123a10391000060092961039103911391014111410151015101411391039103a101411141015101510141015103911141015101510141015101510391015103911391039103a1039103911000d05000000000000000000000000"  # noqa
    pul2_array = [4446, 4568, 487, 1735, 487, 1766, 487, 1735, 487, 639, 487, 639, 487, 609, 487, 639, 487, 639, 487, 1735, 487, 1766, 487, 1735, 487, 639, 487, 609, 517, 609, 487, 639, 487, 639, 487, 609, 517, 1735, 487, 639, 487, 609, 517, 609, 487, 639, 487, 639, 487, 609, 487, 1766, 487, 609, 517, 1735, 487, 1735, 517, 1735, 487, 1674, 548, 1766, 487, 1735, 487, 46776, 4446, 4568, 487, 1735, 487, 1735, 517, 1735, 487, 609, 517, 609, 487, 639, 487, 639, 487, 609, 517, 1735, 487, 1735, 487, 1766, 487, 609, 517, 609, 487, 639, 487, 639, 487, 609, 487, 639, 487, 1735, 517, 609, 487, 639, 487, 639, 487, 609, 487, 639, 487, 639, 487, 1735, 487, 639, 487, 1735, 517, 1735, 487, 1735, 487, 1766, 487, 1735, 487, 1735, 517]  # noqa
    pul2_raw = "4446,4568,487,1735,487,1766,487,1735,487,639,487,639,487,609,487,639,487,639,487,1735,487,1766,487,1735,487,639,487,609,517,609,487,639,487,639,487,609,517,1735,487,639,487,609,517,609,487,639,487,639,487,609,487,1766,487,609,517,1735,487,1735,517,1735,487,1674,548,1766,487,1735,487,46776,4446,4568,487,1735,487,1735,517,1735,487,609,517,609,487,639,487,639,487,609,517,1735,487,1735,487,1766,487,609,517,609,487,639,487,639,487,609,487,639,487,1735,517,609,487,639,487,639,487,609,487,639,487,639,487,1735,487,639,487,1735,517,1735,487,1735,487,1766,487,1735,487,1735,517"  # noqa

    # samsung tv mute
    prot1 = {"protocol": "NECx2", "device": 7, "subdevice": 7, "function": 15}
    prot1_encode = "4512 4512 564 1692 564 1692 564 1692 564 564 564 564 564 564 564 564 564 564 564 1692 564 1692 564 1692 564 564 564 564 564 564 564 564 564 564 564 1692 564 1692 564 1692 564 1692 564 564 564 564 564 564 564 564 564 564 564 564 564 564 564 564 564 1692 564 1692 564 1692 564 1692 564 43992".encode('utf-8')  # noqa
    prot1_res = "4512,4512,564,1692,564,1692,564,1692,564,564,564,564,564,564,564,564,564,564,564,1692,564,1692,564,1692,564,564,564,564,564,564,564,564,564,564,564,1692,564,1692,564,1692,564,1692,564,564,564,564,564,564,564,564,564,564,564,564,564,564,564,564,564,1692,564,1692,564,1692,564,1692,564,43992"  # noqa
    prot1_broadlink = "26004900949413381338133813131313131313131313133813381338131313131313131313131338133813381338131313131313131313131313131313131338133813381338130005a5000d05000000000000000000000000"  # noqa

    prot2 = {"protocol": "NECx1", "device": 16, "subdevice": 16,
             "function": 248}
    prot2_encode = "4512 4512 564 564 564 564 564 564 564 564 564 1692 564 564 564 564 564 564 564 564 564 564 564 564 564 564 564 1692 564 564 564 564 564 564 564 564 564 564 564 564 564 1692 564 1692 564 1692 564 1692 564 1692 564 1692 564 1692 564 1692 564 564 564 564 564 564 564 564 564 564 564 43992 4512 4512 564 1692 564 95880".encode('utf-8')  # noqa
    prot2_res = "4512,4512,564,564,564,564,564,564,564,564,564,1692,564,564,564,564,564,564,564,564,564,564,564,564,564,564,564,1692,564,564,564,564,564,564,564,564,564,564,564,564,564,1692,564,1692,564,1692,564,1692,564,1692,564,1692,564,1692,564,1692,564,564,564,564,564,564,564,564,564,564,564,43992,4512,4512,564,1692,564,95880"  # noqa
    prot2_broadlink = "26005100949413131313131313131338131313131313131313131313131313381313131313131313131313131338133813381338133813381338133813131313131313131313130005a59494133813000c4c000d05000000000000000000000000"  # noqa

    def test__is_broadlink_file(self):
        o = RFIRLuminoThmedia(None, None, None, None)
        self.assertTrue(o._is_broadlink_file("this/is/file"))
        self.assertFalse(o._is_broadlink_file(1))
        self.assertFalse(o._is_broadlink_file({}))
        self.assertFalse(o._is_broadlink_file([]))

    def test__is_protocol(self):
        o = RFIRLuminoThmedia(None, None, None, None)
        self.assertFalse(o._is_protocol("this/is/file"))
        self.assertFalse(o._is_protocol({}))
        self.assertFalse(o._is_protocol([]))
        self.assertFalse(o._is_protocol({"protocol": 1}))
        self.assertFalse(o._is_protocol({"funcion": 2}))
        self.assertFalse(o._is_protocol({"device": 3}))
        self.assertFalse(o._is_protocol({"subdevice": 4}))
        self.assertTrue(o._is_protocol({"protocol": 34,
                                        "function": 23,
                                        "device": 123,
                                        "subdevice": "foo"}))


class TestRFIRLuminoThmedia(TestRFIRLumino):

    def test__broadlink_to_pulesarray(self):
        o = RFIRLuminoThmedia(None, None, None, None)
        self.assertEqual(o._broadlink_to_pulesarray(self.cmd1_broadlink),
                         self.pul1_array)
        self.assertEqual(o._broadlink_to_pulesarray(self.cmd2_broadlink),
                         self.pul2_array)

    # @patch('builtins.open', mock_open(read_data='test'))
    def test__broadlink_file_to_raw(self):
        with patch('builtins.open',
                   mock_open(read_data=self.cmd1_broadlink)) as m:
            o = RFIRLuminoThmedia(None, "/test/folder", None, None)
            self.assertEqual(o._broadlink_file_to_raw("file"),
                             "30,38000," + self.pul1_raw)
            m.assert_called_once_with('/test/folder/file')

        with patch('builtins.open',
                   mock_open(read_data=self.cmd1_broadlink)) as m:
            o = RFIRLuminoThmedia(None, "/test/foo/", None, None, freq=32)
            self.assertEqual(o._broadlink_file_to_raw("file"),
                             "30,32," + self.pul1_raw)
            m.assert_called_once_with('/test/foo/file')

        with patch('builtins.open',
                   mock_open(read_data=self.cmd2_broadlink)) as m:
            o = RFIRLuminoThmedia(None, "/test/folder", None, None)
            self.assertEqual(o._broadlink_file_to_raw("file"),
                             "30,38000," + self.pul2_raw)
            m.assert_called_once_with('/test/folder/file')

        with patch('builtins.open',
                   mock_open(read_data=self.cmd2_broadlink)) as m:
            o = RFIRLuminoThmedia(None, "/test/foo/", None, None, freq=32)
            self.assertEqual(o._broadlink_file_to_raw("file"),
                             "30,32," + self.pul2_raw)
            m.assert_called_once_with('/test/foo/file')

    def test__protocol_to_raw(self):
        with patch('subprocess.check_output',
                   MagicMock(return_value=self.prot1_encode)) as m:
            o = RFIRLuminoThmedia(None, None, None, "encodeir")
            self.assertEqual(o._protocol_to_raw(self.prot1["protocol"],
                                                self.prot1["device"],
                                                self.prot1["subdevice"],
                                                self.prot1["function"]),
                             "30,38000," + self.prot1_res)
            m.assert_called_once_with(["encodeir",
                                       self.prot1["protocol"],
                                       str(self.prot1["device"]),
                                       str(self.prot1["subdevice"]),
                                       str(self.prot1["function"])
                                       ])

        with patch('subprocess.check_output',
                   MagicMock(return_value=self.prot2_encode)) as m:
            o = RFIRLuminoThmedia(None, None, None, "encodeir", freq=42)
            self.assertEqual(o._protocol_to_raw(self.prot2["protocol"],
                                                self.prot2["device"],
                                                self.prot2["subdevice"],
                                                self.prot2["function"]),
                             "30,42," + self.prot2_res)
            m.assert_called_once_with(["encodeir",
                                       self.prot2["protocol"],
                                       str(self.prot2["device"]),
                                       str(self.prot2["subdevice"]),
                                       str(self.prot2["function"])
                                       ])

    def test__get_raw_command(self):
        with patch('subprocess.check_output',
                   MagicMock(return_value=self.prot1_encode)) as m:
            o = RFIRLuminoThmedia(None, None, self.prot1, "my_prog")
            self.assertEqual(o._get_raw_command(),
                             "30,38000," + self.prot1_res)
            self.assertEqual(o._get_raw_command(),
                             "30,38000," + self.prot1_res)
            m.assert_called_once_with(["my_prog",
                                       self.prot1["protocol"],
                                       str(self.prot1["device"]),
                                       str(self.prot1["subdevice"]),
                                       str(self.prot1["function"])
                                       ])

        o = RFIRLuminoThmedia(None, None, {})
        self.assertRaises(Exception, o._get_raw_command)

        with patch('builtins.open',
                   mock_open(read_data=self.cmd1_broadlink)) as m:
            o = RFIRLuminoThmedia(None, "/foo/bar/", "my_cmd", None)
            self.assertEqual(o._get_raw_command(), "30,38000," + self.pul1_raw)
            m.assert_called_once_with('/foo/bar/my_cmd')

    def test_activate(self):
        with patch('subprocess.check_output',
                   MagicMock(return_value=self.prot1_encode)) as m:
            mock_logging = MagicMock()
            mock_mqtt_client = MagicMock()
            o = RFIRLuminoThmedia(mock_mqtt_client, None, self.prot1,
                                  "my_prog", logging=mock_logging)
            o.activate()
            mock_mqtt_client.publish.assert_called_once_with(
                "ir_server/send_1", "30,38000," + self.prot1_res)
            m.assert_called_once_with(["my_prog",
                                       self.prot1["protocol"],
                                       str(self.prot1["device"]),
                                       str(self.prot1["subdevice"]),
                                       str(self.prot1["function"])
                                       ])
            mock_logging.debug.assert_called_once()

        with patch('subprocess.check_output',
                   MagicMock(return_value=self.prot2_encode)) as m:
            m_mqtt_client = MagicMock()
            o = RFIRLuminoThmedia(m_mqtt_client, "foo", self.prot2, "my_prog",
                                  freq=123, channel=55)
            o.activate()
            m_mqtt_client.publish.assert_called_once_with(
                "ir_server/send_55", "30,123," + self.prot2_res)
            m.assert_called_once_with(["my_prog",
                                       self.prot2["protocol"],
                                       str(self.prot2["device"]),
                                       str(self.prot2["subdevice"]),
                                       str(self.prot2["function"])
                                       ])

        with patch('builtins.open',
                   mock_open(read_data=self.cmd1_broadlink)) as m:
            m_mqtt_client = MagicMock()
            o = RFIRLuminoThmedia(m_mqtt_client, "foo/bar/", "my_cmd", None)
            o.activate()
            m_mqtt_client.publish.assert_called_once_with(
                "ir_server/send_1", "30,38000," + self.pul1_raw)
            m.assert_called_once_with('foo/bar/my_cmd')

        with patch('builtins.open',
                   mock_open(read_data=self.cmd2_broadlink)) as m:
            m_mqtt_client = MagicMock()
            o = RFIRLuminoThmedia(m_mqtt_client, "foo", "my_cmd", "my_prog",
                                  freq=123, channel=55)
            o.activate()
            m_mqtt_client.publish.assert_called_once_with(
                "ir_server/send_55", "30,123," + self.pul2_raw)
            m.assert_called_once_with('foo/my_cmd')


class TestRFIRLuminoBroadlink(TestRFIRLumino):

    def test__pulesarray_to_broadlink(self):
        o = RFIRLuminoBroadlink(None, None, None, None)
        self.assertEqual(o._pulesarray_to_broadlink(self.pul1_array, 38000),
                         self.cmd1_broadlink)
        self.assertEqual(o._pulesarray_to_broadlink(self.pul2_array, 38000),
                         self.cmd2_broadlink)

    def test__protocol_to_broadlink(self):
        with patch('subprocess.check_output',
                   MagicMock(return_value=self.prot1_encode)) as m:
            o = RFIRLuminoBroadlink(None, None, None)
            self.assertEqual(o._protocol_to_broadlink(self.prot1["protocol"],
                                                      self.prot1["device"],
                                                      self.prot1["subdevice"],
                                                      self.prot1["function"]),
                             self.prot1_broadlink)
            m.assert_called_once_with(["encodeir",
                                       self.prot1["protocol"],
                                       str(self.prot1["device"]),
                                       str(self.prot1["subdevice"]),
                                       str(self.prot1["function"])
                                       ])

        with patch('subprocess.check_output',
                   MagicMock(return_value=self.prot2_encode)) as m:
            o = RFIRLuminoBroadlink(None, None, None, "my_prog")
            self.assertEqual(o._protocol_to_broadlink(self.prot2["protocol"],
                                                      self.prot2["device"],
                                                      self.prot2["subdevice"],
                                                      self.prot2["function"]),
                             self.prot2_broadlink)
            m.assert_called_once_with(["my_prog",
                                       self.prot2["protocol"],
                                       str(self.prot2["device"]),
                                       str(self.prot2["subdevice"]),
                                       str(self.prot2["function"])
                                       ])

    def test__get_broadlink_command(self):
        o = RFIRLuminoBroadlink(None, None, "tv/bar/foo", None)
        self.assertEqual(o._get_broadlink_command(), "broadlink/tv/bar/foo")

        o = RFIRLuminoBroadlink(None, None, self.prot1, None, signal="rf")
        self.assertRaises(Exception, o._get_broadlink_command)

        o = RFIRLuminoBroadlink(None, None, "tv/bar/foo", None, signal="rf")
        self.assertEqual(o._get_broadlink_command(), "broadlink/tv/bar/foo")

        o = RFIRLuminoBroadlink(None, None, {}, None)
        self.assertRaises(Exception, o._get_broadlink_command)

        o = RFIRLuminoBroadlink(None, None, [])
        self.assertRaises(Exception, o._get_broadlink_command)

        with patch('os.path.exists', MagicMock(return_value=True)) as m:
            o = RFIRLuminoBroadlink(None, "/foo/bar", self.prot1)
            path = (self.prot1["protocol"] + "/" +
                    str(self.prot1["device"]) + "/" +
                    str(self.prot1["subdevice"]) + "/" +
                    str(self.prot1["function"]))
            self.assertEqual(o._get_broadlink_command(), "broadlink/" + path)
            m.assert_called_once_with('/foo/bar/' + path)

    @patch('os.makedirs')
    @patch('os.path.exists')
    @patch('subprocess.check_output')
    def test__get_broadlink_command_prot(self, m_enc, m_exist, m_mkdir):
        m_enc.return_value = self.prot1_encode
        m_exist.return_value = False

        with patch('builtins.open', mock_open()) as m_open:
            o = RFIRLuminoBroadlink(None, "foo/bar", self.prot1)
            path = (self.prot1["protocol"] + "/" +
                    str(self.prot1["device"]) + "/" +
                    str(self.prot1["subdevice"]) + "/" +
                    str(self.prot1["function"]))
            self.assertEqual(o._get_broadlink_command(), "broadlink/" + path)
            m_enc.assert_called_once_with(["encodeir",
                                          self.prot1["protocol"],
                                          str(self.prot1["device"]),
                                          str(self.prot1["subdevice"]),
                                          str(self.prot1["function"])])
            full_path = 'foo/bar/' + path
            m_exist.assert_called_once_with(full_path)
            m_open.assert_called_once_with(full_path, 'w')
            m_mkdir.assert_called_once()
            handle = m_open()
            handle.write.assert_called_once_with(self.prot1_broadlink)

    def test_activate(self):
        mock_logging = MagicMock()
        mock_mqtt_client = MagicMock()
        o = RFIRLuminoBroadlink(mock_mqtt_client, None, "foo/bar",
                                logging=mock_logging)
        o.activate()
        mock_mqtt_client.publish.assert_called_once_with("broadlink/foo/bar",
                                                         "auto")

        mock_mqtt_client = MagicMock()
        o = RFIRLuminoBroadlink(mock_mqtt_client, None, "foo/bar", signal="rf")
        o.activate()
        mock_mqtt_client.publish.assert_called_once_with("broadlink/foo/bar",
                                                         "autorf")
        mock_logging.debug.assert_called_once()


class TestIRRFLuminoList(unittest.TestCase):

    config = {
        1: {'command': 'tv/samsung/volumeup', 'emitter': 'broadlink'},
        2: {'command': 'tv/samsung/channelup', 'emitter': 'broadlink'},
        4: {'command': 'tv/samsung/up', 'emitter': 'broadlink'},
        7: {'command': 'tv/samsung/volumedown', 'emitter': 'broadlink'},
        8: {'command': 'tv/samsung/channeldown', 'emitter': 'broadlink'},
        9: {'command': 'tv/samsung/left', 'emitter': 'broadlink'},
        10: {'command': 'tv/samsung/down', 'emitter': 'broadlink'},
        11: {'command': 'tv/samsung/right', 'emitter': 'broadlink'},
        14: {'command': 'tv/samsung/mute', 'emitter': 'broadlink'},
        25: {'command': 'tv/samsung/power', 'emitter': 'broadlink'},
        26: {'channel': 1, 'command': 'tv/samsung/power',
             'emitter': 'thmedia'},
        28: {'command': 'rfkit/bt1', 'emitter': 'broadlink', 'signal': 'rf'},
        29: {'channel': 2, 'command': 'rfkit/bt2', 'emitter': 'thmedia'},
        30: {'channel': 1, 'command': 'rfkit/bt3', 'emitter': 'thmedia'},
        3: {'command':
            {'protocol': 'NECx2', 'device': 7, 'subdevice': 7, 'function': 15},
            'emitter': 'broadlink'}}

    yaml_file = """
2:
  emitter: broadlink
  command: tv/samsung/channelup
8:
  emitter: broadlink
  command: tv/samsung/channeldown

1:
  emitter: broadlink
  command: tv/samsung/volumeup
7:
  emitter: broadlink
  command: tv/samsung/volumedown
14:
  emitter: broadlink
  command: tv/samsung/mute

4:
  emitter: broadlink
  command: tv/samsung/up
10:
  emitter: broadlink
  command: tv/samsung/down
9:
  emitter: broadlink
  command: tv/samsung/left
11:
  emitter: broadlink
  command: tv/samsung/right

25:
  emitter: broadlink
  command: tv/samsung/power

26:
  emitter: thmedia
  command: tv/samsung/power

28:
  signal: rf
  emitter: broadlink
  command: rfkit/bt1
29:
  emitter: thmedia
  command: rfkit/bt2
  channel: 2
30:
  emitter: thmedia
  command: rfkit/bt3

3:
  emitter: broadlink
  command:
    protocol: NECx2
    device: 7
    subdevice: 7
    function: 15"""

    @patch('RFIRLumino.RFIRLuminoThmedia.activate')
    @patch('RFIRLumino.RFIRLuminoBroadlink.activate')
    def test_activate(self, mock_broadlink, mock_thmedia):
        luminos = RFIRLuminoList(None, "foo/bar", self.config)

        for k in self.config.keys():
            luminos.activate(k)
        luminos.activate(123123)
        self.assertEqual(mock_broadlink.call_count, 12)
        self.assertEqual(mock_thmedia.call_count, 3)

    def test_activate_invalid_emitter(self):
        self.assertRaises(Exception, RFIRLuminoList, None, None,
                          {3: {"emitter": "foo"}})

    @patch('RFIRLumino.RFIRLuminoThmedia.activate')
    @patch('RFIRLumino.RFIRLuminoBroadlink.activate')
    def test_from_yaml_file(self, mock_broadlink, mock_thmedia):
        with patch('builtins.open', mock_open(read_data=self.yaml_file)) as m:
            luminos = RFIRLuminoList.from_yaml_file(None, "foo/bar",
                                                    "test.yaml", None)
            for k in self.config.keys():
                luminos.activate(k)
            self.assertEqual(mock_broadlink.call_count, 12)
            self.assertEqual(mock_thmedia.call_count, 3)
            m.assert_called_once_with('test.yaml')
