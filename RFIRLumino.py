from abc import ABC, abstractmethod
import math
import os
import subprocess
import yaml


class RFIRLumino(ABC):

    def __init__(self, mqtt_client, broadlink_cmd_folder, command,
                 encodeir="encodeir", channel=1, freq=38000, logging=None):
        self.mqtt_client = mqtt_client
        self.channel = channel
        self.command = command
        self.freq = freq
        self.broadlink_cmd_folder = broadlink_cmd_folder
        self.encodeir = encodeir
        self.logging = logging

    @staticmethod
    def _is_broadlink_file(command):
        return isinstance(command, str)

    @staticmethod
    def _is_protocol(command):
        if type(command) is not dict:
            return False
        if ("protocol" not in command or "device" not in command or "subdevice"
           not in command or "function" not in command):
            return False
        return True

    @abstractmethod
    def activate(self):
        pass  # pragma: no cover


class RFIRLuminoThmedia(RFIRLumino):

    def __init__(self, mqtt_client, broadlink_cmd_folder, command,
                 encodeir="encodeir", channel=1, freq=38000, logging=None):
        super().__init__(mqtt_client, broadlink_cmd_folder, command, encodeir,
                         channel, freq, logging)
        self.raw_command = None

    # based on
    # https://github.com/haimkastner/broadlink-ir-converter/blob/master/src/index.ts
    @staticmethod
    def _broadlink_to_pulesarray(broadlinkHexCommand):

        def readNextByte(hexCommand):
            return hexCommand[0:2], hexCommand[2:]

        pulesArray = []
        isIr = False

        currentByte, hexCommand = readNextByte(broadlinkHexCommand)
        if currentByte == '26':
            isIr = True
        # else: Ignore the RF frequency

        # Read the 'repeat byte' (and ignore it)
        currentByte, hexCommand = readNextByte(hexCommand)

        # Read the length bytes (ignore them too)
        currentByte, hexCommand = readNextByte(hexCommand)
        currentByte, hexCommand = readNextByte(hexCommand)

        while True:
            currentByte, hexCommand = readNextByte(hexCommand)

            # If its '00' it means the next hex pules value was larger then one
            # byte. So the broadlink mark by '00' byte that the next byte
            # belong his following byte.
            # For example the 0x123 value will be in shown as '00' '01' '23'
            # so we need to skip the '00' flag and combine the next tow bytes
            # to one hex value
            if currentByte == '00':
                # Read the first byte
                currentByte, hexCommand = readNextByte(hexCommand)
                higherByte = currentByte
                # Read the second byte
                currentByte, hexCommand = readNextByte(hexCommand)
                lowerByte = currentByte
                # Combine the two bytes (big-endian)
                currentByte = higherByte + lowerByte

            # Convert the hex value to a decimal number
            pulsFactor = int(currentByte, 16)
            # Get the pulse lengths (revers the formula: µs * 2^-15))
            puls = pulsFactor / 269 * 8192
            # Flat the pules number
            flatPuls = math.floor(puls)

            pulesArray.append(flatPuls)

            # If the all command converted, or in IR the end flag shown, break
            # the loop
            if not hexCommand or (
               isIr and hexCommand.lower().startswith('000d05')):
                break

        return pulesArray

    def _broadlink_file_to_raw(self, cmd_file):
        with open(os.path.join(self.broadlink_cmd_folder, cmd_file)) as f:
            broadlinkHexCommand = f.read()
            pulesArray = self._broadlink_to_pulesarray(broadlinkHexCommand)

        # Prepend thmedia code "30" for raw data, and frequency
        pulesArray.insert(0, 30)
        pulesArray.insert(1, self.freq)

        return ','.join(map(str, pulesArray))

    def _protocol_to_raw(self, protocol, device, subdevice, function):
        pules = subprocess.check_output([self.encodeir, str(protocol),
                                         str(device), str(subdevice),
                                         str(function)])
        header = "30," + str(self.freq) + ","
        body = pules.decode('utf-8').replace(" ", ",")
        return header + body

    def _get_raw_command(self):
        # always convert files to raw, in case the file got updated
        if self._is_broadlink_file(self.command):
            return self._broadlink_file_to_raw(self.command)

        if self.raw_command:
            return self.raw_command

        if not self._is_protocol(self.command):
            raise Exception("command or signal type not compatible")

        self.raw_command = self._protocol_to_raw(self.command["protocol"],
                                                 self.command["device"],
                                                 self.command["subdevice"],
                                                 self.command["function"])
        return self.raw_command

    def activate(self):
        msg = self._get_raw_command()
        topic = "ir_server/send_" + str(self.channel)
        self.mqtt_client.publish(topic, msg)
        if self.logging:
            self.logging.debug('{:>10}: {} | {}'.format('MQTT', topic,  msg))


class RFIRLuminoBroadlink(RFIRLumino):

    def __init__(self, mqtt_client, broadlink_cmd_folder, command,
                 encodeir="encodeir", channel=1, freq=38000, signal="ir",
                 logging=None):
        super().__init__(mqtt_client, broadlink_cmd_folder, command, encodeir,
                         channel, freq, logging)
        self.command_file = None
        self.signal = signal

    @staticmethod
    def _pulesarray_to_broadlink(pulesArray, freq):
        broadlink_header = ''
        broadlink_body = ''

        # IR frequency
        broadlink_header += "{:02x}".format(round(freq/1000))
        # Add don't repeat byte flag
        broadlink_header += '00'

        for i in pulesArray:
            puls = round(i * 269 / 8192)
            hexCommand = "{:02x}".format(puls)
            if len(hexCommand) > 2:
                # if it can't stored in one byte, pad it to double byte
                # and add the '00' flag (this flag mark that the next value is
                # two bytes and not only one)
                hexCommand = "00" + "{:04x}".format(puls)

            # Add the command to the final broadlink command
            broadlink_body += hexCommand

        # Add length to the header (with adicional 3 for OFF constant of the
        # footer)
        length = "{:04x}".format(int(len(broadlink_body) / 2) + 3)
        # change to little endien
        broadlink_header += length[2:] + length[:2]
        # Add the IR command ends flag (for IR only)
        broadlink_footer = "000d05000000000000000000000000"

        return broadlink_header + broadlink_body + broadlink_footer

    def _protocol_to_broadlink(self, protocol, device, subdevice, function):
        pules = subprocess.check_output([self.encodeir, str(protocol),
                                         str(device), str(subdevice),
                                         str(function)])
        pulesarray = [int(x) for x in pules.split()]
        return self._pulesarray_to_broadlink(pulesarray, self.freq)

    def _get_broadlink_command(self):
        if self._is_broadlink_file(self.command):
            return os.path.join("broadlink/", self.command)

        if not self._is_protocol(self.command) or self.signal != "ir":
            raise Exception("command or signal type not compatible")

        self.command_file = os.path.join(str(self.command["protocol"]),
                                         str(self.command["device"]),
                                         str(self.command["subdevice"]),
                                         str(self.command["function"]))
        realpath = os.path.join(self.broadlink_cmd_folder, self.command_file)
        if os.path.exists(realpath):
            return os.path.join("broadlink/", self.command_file)

        broadlink_hex = self._protocol_to_broadlink(self.command["protocol"],
                                                    self.command["device"],
                                                    self.command["subdevice"],
                                                    self.command["function"])
        os.makedirs(os.path.dirname(realpath), exist_ok=True)
        with open(realpath, 'w') as f:
            f.write(broadlink_hex)

        return os.path.join("broadlink/", self.command_file)

    def activate(self):
        if self.signal == "ir":
            msg = "auto"
        else:
            msg = "autorf"
        topic = self._get_broadlink_command()
        self.mqtt_client.publish(topic, msg)
        if self.logging:
            self.logging.debug('{:>10}: {} | {}'.format('MQTT', topic,  msg))


class RFIRLuminoList:

    def __init__(self, mqtt_client, broadlink_cmd_folder, config,
                 encodeir="encodeir", logging=None):
        self.luminos = {}
        if not config:
            return
        for k, i in config.items():
            if i["emitter"].lower() == "broadlink":
                self.luminos[k] = RFIRLuminoBroadlink(mqtt_client,
                                                      broadlink_cmd_folder,
                                                      i["command"],
                                                      encodeir,
                                                      i.get("channel", 1),
                                                      i.get("freq", 38000),
                                                      i.get("signal", "ir"),
                                                      logging)
            elif i["emitter"].lower() == "thmedia":
                self.luminos[k] = RFIRLuminoThmedia(mqtt_client,
                                                    broadlink_cmd_folder,
                                                    i["command"],
                                                    encodeir,
                                                    i.get("channel", 1),
                                                    i.get("freq", 38000),
                                                    logging)
            else:
                raise Exception("Invalid emitter")

    @classmethod
    def from_yaml_file(cls, mqtt_client, broadlink_cmd_folder, yaml_file,
                       encodeir, logging=None):
        try:
            with open(yaml_file) as f:
                config = yaml.load(f, Loader=yaml.FullLoader)
        except FileNotFoundError:
            config = None
        return cls(mqtt_client, broadlink_cmd_folder, config, encodeir,
                   logging)

    def activate(self, lumino):
        lum = self.luminos.get(lumino, None)
        if lum:
            lum.activate()
