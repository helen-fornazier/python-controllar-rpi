#!/usr/bin/env python3

from common_utils import *
from threading import Thread
from threading import Event
from time import sleep
import re
from RFIRLumino import RFIRLuminoList
import paho.mqtt.client as paho
import math

# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

# TODO: see if it is better to use local var instead
rfirlumino = None


def get_state_of_lights(lights):
    commands = b''
    for key, value in lights.items():
        if value[2] > 0 and not is_ipcommand(key[0], key[1]):
            commands += compile_command_string_socket(key, value)
    #Divino was here!

    return commands


# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=


def serial_backdoor(command_string):
    if NEW_SYSTEM:
        serial_command = command_string[1:]
        raspberry_serial.write(serial_command)
        raspberry_serial.write(bytes('\n\r', 'utf-16be'))
        return serial_command


# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=


def listen_to_raspberry_serial(raspberry_serial, lights, clients, clientsCommandIP,
                                irclient):
    logging.info('Raspberry Serial Listen Thread started (Main Thread)')
    changes = []
    all = []
    dead_clients = []

    while True:
        buffered_data = b''
        if not NEW_SYSTEM:
            raspberry_serial.write(b'Z')
        while raspberry_serial.inWaiting() > 0:
            buffered_data += raspberry_serial.read(1)

        if buffered_data:
            logging.debug('{:>10}: {}'.format('BUFFER', buffered_data))

        commands = re.findall(serial_command_pattern, buffered_data)

        for command_string in commands:
            logging.debug('{:>10}: {}'.format('REDE', command_string))
            try:
                kv = parse_command_string(command_string)
                all.append(kv)
                if NEW_SYSTEM:                      # dimmer hack
                    key = kv[0]
                    new_intens = kv[1]
                    old_value = lights[key]
                    if is_dimmer(old_value):        # button is dimmer
                        if new_intens == 0:         # dimmer was switched off
                            onoff = 0               # set onoff to 0
                                                    # preserve previsou intensity
                            new_value = onoff, old_value[1], old_value[2], old_value[3]
                            lights[key] = new_value
                        elif new_intens > 0:        # dimmer is on
                            onoff = 1               # set/keep onoff on 1
                                                    # save new intensity
                            new_value = onoff, old_value[1], new_intens, old_value[3]
                            lights[key] = new_value
                        changes.append(key)
                    elif old_value[2] != new_intens:    # button is not dimmer and changed
                                                    # save new intensity
                        new_value = old_value[0], old_value[1], new_intens, old_value[3]
                        lights[key] = new_value
                        changes.append(key)
                    if config:
                        for externals in config:
                            sendMessage(
                                buildMessage(kv[0][0], kv[0][1], kv[1], externals['Commands']), 
                                externals['Protocol'], 
                                externals['config'])

                else:
                    # Save the lights that changed
                    if kv[0] not in lights or lights[kv[0]] != kv[1]:
                        lights[kv[0]] = kv[1]
                        changes.append(kv[0])
            except ValueError:
               logging.error('{:>10}: {}'.format('UNKNOWN', command_string))
            except Exception:
                logging.exception('Listen to Serial Error')


        # Send the changes to clients commandIP and IRclient
        for kv in all:
            # TODO: check if here is the appropriated place
            lumino =  '{:03d}{:02d}'.format(kv[2], kv[0][1], kv[1]).encode(UTF_8)
            rfirlumino.activate(int(lumino))

            bypass_string =  '$INF,{:03d}{:02d},{:02d}\r\n'.format(kv[2], kv[0][1], kv[1]).encode(UTF_8)
            logging.debug('{:>10}: {}'.format('bypass', bypass_string))
            for client in clientsCommandIP:
                try:
                    client.sendall(bypass_string)
                except BrokenPipeError:
                    dead_clients.append(client)
            for dc in dead_clients:
                if dc in clientsCommandIP:
                    logging.error('Client CommandIP has broken pipe, removing from list')
                    clientsCommandIP.remove(dc)
            dead_clients.clear()
                
        # Send the changes to clients
        for key in changes:
            value = lights[key]
            compiled_string = compile_command_string_socket(key, value)
            logging.debug('{:>10}: {}'.format('FEEDBACK', compiled_string))
            for client in clients:
                try:
                    client.sendall(compiled_string)
                except BrokenPipeError:
                    dead_clients.append(client)
            for dc in dead_clients:
                if dc in clients:
                    logging.error('Client has broken pipe, removing from list')
                    clients.remove(dc)
            dead_clients.clear()
        changes.clear()
        all.clear()
        sleep(serial_loop_delay)
    logging.exception('Raspberry Serial Listen Thread stopped (Main Thread)')


# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=


def handle_request(client_socket, raspberry_serial, clients, lights):
    # Receive a single client request on `sock` and send the answer.

    buffered_data = b''
    while True:
        commands = receive_commands(client_socket, buffered_data)
        buffered_data = commands.pop()

        for command_string in commands:
            # scene ends with '\r\n\n', resolve with some cool regex
            command_string = re.sub(b'\r', b'', command_string)
            if command_string == b'':
                continue
            elif chr(command_string[0]) == 'M':
                # get Server Id
                if client_socket in clients:
                    clients.remove(client_socket)
                send_to_android_for_installation(client_socket, webserver_id.encode(
                    UTF_8))
            elif chr(command_string[0]) == 'V':
                if client_socket in clients:
                    clients.remove(client_socket)
                # get Database Hash
                send_to_android_for_installation(client_socket, get_database_hash(database_file))
            elif chr(command_string[0]) == 'D':
                # get Database
                if client_socket in clients:
                    clients.remove(client_socket)
                send_to_android_for_installation(client_socket, get_database(database_file))
            elif chr(command_string[0]) == 'S':
                # get Current State of Lights
                commands = get_state_of_lights(lights)
                logging.debug('{:>10}: {}'.format('STATES', commands))
                client_socket.sendall(commands)
                clients.add(client_socket)
            elif chr(command_string[0]) == 'B':
                # Send Serial commands through SSH
                commands = serial_backdoor(command_string)
                logging.debug('{:>10}: {}'.format('SENT TO SERIAL', commands))
            else:
                lumino = process_command_from_socket(command_string, raspberry_serial, lights)
                rfirlumino.activate(int(lumino))
                sleep(serial_write_delay)


# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=


def handle_conversation(client_socket, raspberry_serial, clients, lights, address):
    # Converse with a client over `sock` until they are done talking.
    try:
        handle_request(client_socket, raspberry_serial, clients, lights)
    except EOFError:
        # correctly closed connection from client
        logging.info('Client socket to {} has closed'.format(address))
    except TimeoutError:
        # this error happens when listening on disconnected client socket
        logging.error('ERROR: Client socket to {} has timed out'.format(address))
    except BrokenPipeError:
        # this error happens when sending to disconnected client socket
        logging.error('ERROR: Client socket to {} has broken pipe'.format(address))
    except Exception:
        logging.exception('Client {} error'.format(address))
    finally:
        if client_socket in clients:
            clients.remove(client_socket)
            logging.info('Removing closed socket from clients list: {}'.format(address))
        client_socket.close()


def accept_connections_forever(listener, raspberry_serial, clients, lights):
    # Forever answer incoming connections on a listening socket.
    while True:
        client_socket, address = listener.accept()
        logging.info('Accepted connection from {}'.format(address[0]))
        logging.info('Client was added to clients list: {}'.format(client_socket.getpeername()[0]))
        clients.add(client_socket)
        handle_conversation(client_socket, raspberry_serial, clients, lights, address[0])


def start_server_threads(listener, raspberry_serial, clients, lights, workers):
    args_tuple = (listener, raspberry_serial, clients, lights)
    for i in range(workers):
        logging.info('Starting Server Client Listening Thread {}'.format(i))
        Thread(target=accept_connections_forever, args=args_tuple, daemon=True).start()


def start_irclient_thread(irclient):
        Thread(target=irclient.loop_forever, daemon=True).start()


def create_server_socket(address):
    # Build and return a listening server socket.
    listener = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    listener.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    listener.bind(address)
    listener.listen(64)
    logging.info('Server is listening at interface {}'.format(address))
    return listener


# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

# AMAZON THREAD

def listen_amazon_server(raspberry_serial, clients, lights):
    logging.info('Amazon Listen Thread started')
    retries_counter = 0
    amazon_socket = None
    heartbeat_thread = None
    while True:
        try:
            logging.info('Creating Amazon socket')
            amazon_socket = connect_to_server(amazon_address)
            logging.info('Registering device to Amazon server')
            send_serial = 'W' + webserver_id + '\n\r'
            amazon_socket.sendall(send_serial.encode(UTF_8))
            logging.info('Amazon socket was added to clients list')
            clients.add(amazon_socket)

            heartbeat_stop_event = Event()
            heartbeat_thread = Thread(target=heartbeat_amazon_server,
                                    args=(amazon_socket, heartbeat_stop_event),
                                    daemon=True)
            logging.info('Starting Amazon Heartbeat Thread')
            heartbeat_thread.start()

            retries_counter = 0     # after successful connect reset counter

            buffered_data = b''
            while True:
                commands = receive_commands(amazon_socket, buffered_data)
                buffered_data = commands.pop()

                for command_string in commands:
                    # scene ends with '\r\n\n', resolve with some cool regex
                    command_string = re.sub(b'\r', b'', command_string)
                    if command_string == b'':
                        continue
                    elif chr(command_string[0]) == 'S':
                        commands = get_state_of_lights(lights)
                        logging.debug('{:>10}: {}'.format('STATES', commands))
                        amazon_socket.sendall(commands)
                    elif chr(command_string[0]) == 'B':
                        # Send Serial commands through SSH
                        commands = serial_backdoor(command_string)
                        logging.debug('{:>10}: {}'.format('SENT TO SERIAL', commands))
                    else:
                        lumino = process_command_from_socket(command_string, raspberry_serial, lights)
                        rfirlumino.activate(int(lumino))
                        if len(commands) > 1:
                            sleep(serial_write_delay)
        except TimeoutError:
            logging.error('Amazon socket has timed out')
        except BrokenPipeError:
            logging.error('Amazon socket has broken the pipe')
        except Exception:
            logging.exception('Amazon Listen Thread error')
        finally:
            if heartbeat_thread is not None and heartbeat_thread.is_alive():
                logging.info('Stopping Amazon Heartbeat Thread')
                heartbeat_stop_event.set()
            if amazon_socket is not None:
                if amazon_socket in clients:
                    logging.info('Removing Amazon socket from clients list')
                    clients.remove(amazon_socket)
                logging.info('Closing Amazon socket')
                amazon_socket.close()
        if retries_counter > amazon_retries_limit:
            Event().wait(retry_wait)
        retries_counter += 1
        logging.info('Amazon connection retry: {}'.format(retries_counter))


def heartbeat_amazon_server(amazon_socket, heartbeat_stop_event):
    logging.info('Amazon Heartbeat Thread started')
    heartbeat_stop_event.wait(heartbeat_wait)
    intens = 0
    while not heartbeat_stop_event.is_set():
        intens = 50 if intens == 0 else 0
        heartbeat = compile_command_string_socket((128, 31), (1, 0, intens))
        amazon_socket.sendall(heartbeat)
        heartbeat_stop_event.wait(heartbeat_wait)
    logging.info('Amazon Heartbeat Thread finished')


# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

if __name__ == '__main__':

    logging.info('STARTING CONTROLLAR WEBSERVER')

    try:
        with open('/controllar/config.json', "r") as json_data_file:
            config = json.loads(json_data_file.read())
            logging.info('TCP/IP COMMAND ENABLED.')
    except FileNotFoundError:
        config = None
        logging.info('NO TCP/IP COMMANDS CONFIGURED')

    if NEW_SYSTEM:
        logging.info('NEW SYSTEM')
        lights = load_lights_types(database_file)
    else:
        logging.info('OLD SYSTEM')
        lights = {}

    clients = set()
    clients_commandIP = set()

    irclient = paho.Client("raspberrypi")
    irclient.connect(mqtt_broker, mqtt_broker_port)
    rfirlumino = RFIRLuminoList.from_yaml_file(irclient, broadlink_cmd_folder,
                                               irconfig_file, encodeir,
                                               logging)

    # Connect to raspberry
    logging.info('Creating Raspberry socket')
    raspberry_serial = connect_to_serial(serial_device)

    if remote_access:
        # Connect to Amazon
        logging.info('Starting Amazon Listen Thread')
        Thread(target=listen_amazon_server, args=(raspberry_serial, clients, lights), daemon=True).start()

    # Start accepting clients from cellphones
    listener = create_server_socket(interface_address)
    start_server_threads(listener, raspberry_serial, clients, lights, workers)

    listenerIPCommand = create_server_socket(interface_address_command_ip)
    start_server_threads(listenerIPCommand, raspberry_serial, clients_commandIP, lights, workers)

    # Start irclient thread
    start_irclient_thread(irclient)

    # Start raspberry listening thread with lights dict
    try:
        logging.info('Starting Raspberry Serial Listen Thread (Main Thread)')
        listen_to_raspberry_serial(raspberry_serial, lights, clients, clients_commandIP,
                                   irclient)
    except:
        logging.exception('MAIN THREAD ERROR')

    logging.exception('MAIN THREAD UNEXPECTEDLY FINISHED')


# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

