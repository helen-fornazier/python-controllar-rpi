#!/bin/bash

choice="n"
   
while [ "$choice" != "s" -a "$choice" != "S" -a "$choice" != "Sim" -a "$choice" != "sim" ]
do
    clear
    echo "-----------------------"
    echo "Bem vindo(a) ao programa de configuracao da Controllar"
    echo "Digite 1 para configurar o WebServer. Ou digite 2 para configurar a ShowCase:"
    read configuracao
    echo "Digite 1 para Laranja, 2 para Raspberry Pi2 e 3 para Raspberry Pi3:"
    read server
    echo "Digite 1 para Sistema Novo e 2 para Sistema Velho:"
    read system
    if [ "$configuracao" = "2" ]
    then
	    echo "Escreva o codigo de Identificacao da ShowCase:"
    	read identification
	fi
    echo ""
    echo "Esta configuracao esta correta?"
    
    case $configuracao in
	1) echo "WebServer";;
	
	2) echo "ShowCase";;
	
	*) echo "Erro de Configuracao: #001"
	   exit 1;;
	
    esac
    
    case $server in
	1) echo "Laranja"
	   server="/dev/ttyS3";;
	  
	2) echo "Raspberry Pi2"
	   server="/dev/ttyAMA0";;
	
	3) echo "Raspberry Pi3"
	   server="/dev/serial0";;
	
	*) echo "Erro de Configuracao: #002"
	   exit 1;;
    esac 
    
    case $system in
	1) echo "Sistema Novo"
	   system="yes";;
	2) echo "Sistema Velho"
	   system="no";;
	*) echo "Erro de Configuracao: #003"
	   exit 1;;
    esac
    
    echo $identification

    echo ""
    echo "Sim/Nao"
    
    read choice
    
done


echo "Arquivo de Configuracao sendo criado. . . ."

rm config.ini
touch config.ini

file=`find ./  -printf "%f\n"| grep -v app.db3 | grep -m1 "\.db3"`

echo "[DEFAULT]" > config.ini
echo "new_system = $system" >> config.ini
echo "database_file = `pwd`/$file" >> config.ini
if [ "$configuracao" = "2" ]
then
	update-rc.d isc-dhcp-server enable
	update-rc.d hostapd enable
	echo "" > /var/spool/cron/crontabs/root
	echo "webserver_id = ShowCase$identification" >> config.ini
	echo "serial_device = $server" >> config.ini
	echo "serial_write_delay = 0.02" >> config.ini
	echo "serial_loop_delay = 0.5" >> config.ini
else
	echo "webserver_id = $file" | sed 's/.db3//' >> config.ini
	echo "serial_device = $server" >> config.ini
	echo "serial_write_delay = 0.05" >> config.ini
	echo "serial_loop_delay = 0.01" >> config.ini
fi


echo "mqtt_broker = localhost" >> config.ini
echo "mqtt_broker_port = 1883" >> config.ini
echo "irconfig_file = `pwd`/$file" | sed 's/.db3/.yaml/' >> config.ini
echo "broadlink_cmd_folder = /controllar/broadlink-mqtt/commands" >> config.ini
echo "encodeir = /controllar/MakeHex/encodeir" >> config.ini

echo "server_interface = 0.0.0.0" >> config.ini
echo "server_port = 9000" >> config.ini
echo "workers = 10" >> config.ini
echo "remote_access = yes" >> config.ini
echo "amazon_host = aplicacao.controllar.com" >> config.ini
echo "amazon_port = 8900" >> config.ini
echo "heartbeat_wait = 60" >> config.ini
echo "amazon_retries_limit = 3" >> config.ini
echo "retry_wait = 60" >> config.ini
echo "log_file = /controllar/server.log" >> config.ini

if [ "$configuracao" = "2" ]
then
    touch hostapd.conf
    echo 'driver=nl80211' > hostapd.conf
    echo 'interface=wlan0' >> hostapd.conf
    echo 'hw_mode=g' >> hostapd.conf
    echo 'channel=6' >> hostapd.conf
    echo 'ieee80211d=1' >> hostapd.conf
    echo 'country_code=BR' >> hostapd.conf
    echo 'ieee80211n=1' >> hostapd.conf
    echo 'wmm_enabled=1' >> hostapd.conf
    printf 'ssid=ShowCase' >> hostapd.conf
    echo $identification >> hostapd.conf
    echo 'wpa_passphrase=showcasecontrollar' >> hostapd.conf
    echo 'ht_capab=[HT40][SHORT-GI-20][DSSS_CCK-40]' >> hostapd.conf
    echo 'macaddr_acl=0' >> hostapd.conf
    echo 'ignore_broadcast_ssid=0' >> hostapd.conf
    echo 'auth_algs=1' >> hostapd.conf
    echo 'wpa=2' >> hostapd.conf
    echo 'wpa_key_mgmt=WPA-PSK' >> hostapd.conf
    echo 'rsn_pairwise=CCMP' >> hostapd.conf
    rm /etc/hostapd/hostapd.conf
    mv hostapd.conf /etc/hostapd/hostapd.conf
fi

rm server.log
touch server.log
chmod 755 server.log
echo "Configuracao Terminada com Sucesso!"


exit 0
